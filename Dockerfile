FROM php:7.3.4-cli-alpine
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ARG PHPSTAN_VERSION

ENV COMPOSER_VERSION="1.8.5"
ENV COMPOSER_ALLOW_SUPERUSER="1"

RUN apk add --no-cache curl && \
	curl -sL -o composer.phar https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar && \
	mv composer.phar /usr/local/bin/composer && \
	chmod +x /usr/local/bin/composer && \
	composer global require \
		phpstan/phpstan:${PHPSTAN_VERSION} \
		phpstan/phpstan-strict-rules:0.11 \
		phpstan/phpstan-nette:0.11 \
		phpstan/phpstan-doctrine:0.11.2 \
		phpstan/phpstan-phpunit:0.11 \
		phpstan/phpstan-mockery:0.11 \
		phpstan/phpstan-beberlei-assert:0.11 && \
	mkdir -p /app

WORKDIR /app
VOLUME /app
ENTRYPOINT ["/root/.composer/vendor/bin/phpstan"]
